# CDCGAN
Implementace CDCGAN modelu, který generuje dermoskopické obrázky z [datasetu ISIC 2019](https://challenge2019.isic-archive.com/).

## Model ke stažení
Model je ke stažení na adrese [https://drive.google.com/file/d/1tgdkezcUtAObq0GojXiV_yWTpxw0BZTA/view?usp=sharing](https://drive.google.com/file/d/1tgdkezcUtAObq0GojXiV_yWTpxw0BZTA/view?usp=sharing). Tento model byl trénován po dobu 30 epoch.

## Trénování

* spustit trénování:

```bash
python train.py \
--images_dir ./dataset/images \
--log_dir ./logs \
--batch_size 128 \
--epochs 30 \
--saved_models_dir ./logs/saved_models \
--save_model_freq 1 \
--saved_checkpoints_dir ./logs/saved_checkpoints \
--generate_images_freq 66 \
--save_checkpoint_freq 66 \
--gen_lr 0.0001 \
--dis_lr 0.0001
```

* obnovit trénování:

```bash
python train.py \
--images_dir ./dataset/images \
--log_dir ./logs \
--batch_size 128 \
--epochs 30 \
--saved_models_dir ./logs/saved_models \
--save_model_freq 1 \
--saved_checkpoints_dir ./logs/saved_checkpoints \
--generate_images_freq 66 \
--save_checkpoint_freq 66 \
--gen_lr 0.0001 \
--dis_lr 0.0001 \
--load_checkpoint ./logs/saved_checkpoints/ckpt-462
```

## Evaluace
Pomocí skriptu `evaluate.py` lze pomocí metrik FID a Intra FID ohodnotit modely CDCGAN, 
[Pix2Pix](https://gitlab.com/mpribyl/pix2pix) a [StyleGAN2ADA](https://github.com/NVlabs/stylegan2-ada-pytorch). Pro 
ohodnocení modelu StyleGAN2ADA je však třeba mít k dispozici jeji 
kód z repositáře [https://github.com/NVlabs/stylegan2-ada-pytorch](https://github.com/NVlabs/stylegan2-ada-pytorch).

* uložit statistiky datasetu, potřebné pro metriky FID a Intra FID:

```bash
python evaluate.py calc_dataset_statistics \
--images_dir ./dataset/images \
--batch_size 32 \
--output_dir ./dataset_statistics
```

* ohodnotit uložené CDCGAN modely:

```bash
python evaluate.py calc_all_fid_and_intra_fid \
--dataset_statistics_dir ./dataset_statistics \
--models_dir ./logs/saved_models \
--batch_size 128 \
--metrics_dir ./logs/metrics \
--model_type cdcgan
```

* ohodnotit uložené Pix2Pix modely:

```bash
python evaluate.py calc_all_fid_and_intra_fid \
--dataset_statistics_dir ./dataset_statistics \
--models_dir ./logs/saved_models \
--batch_size 128 \
--metrics_dir ./logs/metrics \
--input_images_dir ./dataset/pix2pix/input_images \
--model_type pix2pix
```

* ohodnotit uložené StyleGAN2ADA modely:

```bash
python evaluate.py calc_all_fid_and_intra_fid \
--dataset_statistics_dir ./dataset_statistics \
--models_dir ./logs/saved_models \
--batch_size 64 \
--metrics_dir ./logs/metrics \
--path_to_code ./stylegan2-ada-pytorch \
--model_type stylegan2ada
```

## Augmentace dat
Pomocí skriptu `generate.py` je možné vytvořit augmentovaná data pomocí modelů
[Pix2Pix](https://gitlab.com/mpribyl/pix2pix) a [StyleGAN2ADA](https://github.com/NVlabs/stylegan2-ada-pytorch)

* vytvoření augmentovaných dat pomocí modelu Pix2Pix:

```bash
python generate.py pix2pix_images \
--input_images_dir ./dataset/pix2pix/input_images \
--models_filepath ./pix2pix/logs/saved_models/epoch_29.h5 \
--output_dir ./augmented_data/pix2pix/ \
--batch_size 64 \
--num_images 1000
```

* vytvoření augmentovaných dat pomocí modelu StyleGAN2ADA:

```bash
python generate.py stylegan2ada_images \
--path_to_code ./stylegan2-ada-pytorch \
--models_filepath ./stylegan2ada/logs/saved_models/epoch_29.pkl \
--output_dir ./augmented_data/stylegan2ada/ \
--batch_size 16 \
--num_images 1000
```
