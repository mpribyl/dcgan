import argparse
import json
import os
import time

from dotmap import DotMap

from training.dataset import NUM_LABELS, get_filepaths_per_class


def process_input_arguments_and_exec_command() -> DotMap:
    ap = argparse.ArgumentParser()
    subparsers = ap.add_subparsers(help="commands", dest="command", required=True)

    stylegan_parser = subparsers.add_parser('stylegan2ada_images', help="Generate images with StyleGAN 2 ADA")
    stylegan_parser.add_argument("--output_dir", type=str, required=True,
                                 help="Path to output directory, where the images will be saved.")
    stylegan_parser.add_argument("--num_images", type=int, required=True,
                                 help="Num images which will be created for each class.")
    stylegan_parser.add_argument("--batch_size", type=int, default=32,
                                 help="Num images which will be processed at one time.")
    stylegan_parser.add_argument("--target_img_width", type=int, default=224, help="Target image width.")
    stylegan_parser.add_argument("--target_img_height", type=int, default=224, help="Target image height.")
    stylegan_parser.add_argument("--models_filepath", type=str, required=True, help="Path to model's directory.")
    stylegan_parser.add_argument("--path_to_code", required=True, type=str, help="Path to code for StyleGAN 2 ADA.")
    stylegan_parser.set_defaults(func=generate_images_stylegan2ada)

    pix2pix_parser = subparsers.add_parser('pix2pix_images', help="Generate images with StyleGAN 2 ADA")
    pix2pix_parser.add_argument("--output_dir", type=str, required=True,
                                help="Path to output directory, where the images will be saved.")
    pix2pix_parser.add_argument("--num_images", type=int, required=True,
                                help="Num images which will be created for each class.")
    pix2pix_parser.add_argument("--batch_size", type=int, default=32,
                                help="Num images which will be processed at one time.")
    pix2pix_parser.add_argument("--target_img_width", type=int, default=224, help="Target image width.")
    pix2pix_parser.add_argument("--target_img_height", type=int, default=224, help="Target image height.")
    pix2pix_parser.add_argument("--models_filepath", type=str, required=True, help="Path to model's directory.")
    pix2pix_parser.add_argument("--input_images_dir", type=str, help="Path to input images directory.")
    pix2pix_parser.set_defaults(func=generate_images_pix2pix)

    args = ap.parse_args()
    params = DotMap(vars(args))
    del params['func']
    print('Input params:')
    print(json.dumps(params, indent=2))

    os.makedirs(params.output_dir, exist_ok=True)

    del params['command']
    start_time = time.time()
    args.func(**params)
    print('Result images are now in folder {}.'.format(params.output_dir))
    print('Images was created after {} sec.'.format(time.time() - start_time))

    return params


def generate_images_stylegan2ada(output_dir, num_images, batch_size, path_to_code, models_filepath,
                                 target_img_width, target_img_height):
    import sys
    sys.path.insert(0, path_to_code)
    import torch
    import dnnlib
    import legacy
    import torch.nn.functional as F
    from torchvision.transforms import ToPILImage

    device = torch.device('cuda')
    with dnnlib.util.open_url(models_filepath) as f:
        generator = legacy.load_network_pkl(f)['G_ema'].to(device)

    counter = 0
    for clazz in range(NUM_LABELS):
        batches = [len(range(num_images)[i:i + batch_size]) for i in range(0, len(range(num_images)), batch_size)]
        for batch in batches:
            z = torch.randn([batch, generator.z_dim]).to(device)
            c = torch.zeros([batch, generator.c_dim]).to(device)
            c[:, clazz] = 1

            images = generator(z, c, truncation_psi=1, noise_mode='const', force_fp32=True)
            images = (images * 127.5 + 128).clamp(0, 255).to(torch.uint8)
            images = F.interpolate(images, size=(target_img_width, target_img_height))

            for input_image in images:
                filepath = os.path.join(output_dir, '{}_gan_img_{}.jpg'.format(clazz, counter))
                ToPILImage()(input_image).save(filepath, mode='jpg')
                counter = counter + 1


def generate_images_pix2pix(output_dir, num_images, batch_size, input_images_dir, models_filepath,
                            target_img_width, target_img_height):
    import tensorflow as tf
    import glob
    import numpy as np

    generator = tf.keras.models.load_model(models_filepath, compile=False)

    @tf.function
    def generate_images(input_imgs):
        generated_images = generator(input_imgs, training=True)
        generated_images = tf.multiply(generated_images, 0.5)
        generated_images = tf.add(generated_images, 0.5)
        generated_images = tf.image.resize_with_crop_or_pad(generated_images,
                                                            target_width=target_img_width,
                                                            target_height=target_img_height)
        return generated_images

    all_filepaths = glob.glob(os.path.join(input_images_dir, '*'))
    filepaths_per_class = get_filepaths_per_class(all_filepaths)

    filepaths_for_processing = []
    for clazz, filepaths in filepaths_per_class.items():
        replace = True if len(filepaths) < num_images else False
        filepaths_for_processing.extend(np.random.choice(filepaths, size=num_images, replace=replace))

    filepaths_batches = [filepaths_for_processing[i:i + batch_size] for i in
                         range(0, len(filepaths_for_processing), batch_size)]

    counter = 0
    for filepaths_batch in filepaths_batches:
        input_images = []
        labels = []

        for filepath in filepaths_batch:
            input_image = tf.io.read_file(filepath)
            input_image = tf.image.decode_jpeg(input_image)
            input_image = tf.cast(input_image, dtype=tf.float32)
            input_image = (input_image / 127.5) - 1
            input_images.append(input_image)
            labels.append(os.path.basename(filepath).split('_')[0])

        images = generate_images(tf.convert_to_tensor(tf.convert_to_tensor(input_images)))

        for image, label in zip(images, labels):
            filepath = os.path.join(output_dir, '{}_aug_img_{}.jpg'.format(label, counter))
            tf.keras.preprocessing.image.save_img(filepath, image)
            counter = counter + 1


def main():
    process_input_arguments_and_exec_command()


if __name__ == '__main__':
    main()
