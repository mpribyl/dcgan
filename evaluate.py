import argparse
import json

from dotmap import DotMap

from evaluation.fid import calc_fid_from_statistics, calc_all_fid_from_statistics
from evaluation.statistics import calc_and_save_statistics


def process_input_arguments_and_execute_command() -> DotMap:
    ap = argparse.ArgumentParser()
    subparsers = ap.add_subparsers(help="commands", dest="command", required=True)

    parser_calc_fids = subparsers.add_parser('calc_fid_and_intra_fid',
                                             help="Calculate FID and Intra-FID for statistics.")
    parser_calc_fids.add_argument("--dataset_statistics_dir",
                                  type=str, required=True,
                                  help="Statistics filepath.")
    parser_calc_fids.add_argument("--models_filepath",
                                  type=str, required=True,
                                  help="Path to model's directory.")
    parser_calc_fids.add_argument("--model_type",
                                  type=str, required=True, choices=['cdcgan', 'pix2pix', 'stylegan2ada'],
                                  help="Type of generator.")
    parser_calc_fids.add_argument("--input_images_dir",
                                  type=str,
                                  help="Path to input images directory for pix2pix model.")
    parser_calc_fids.add_argument("--path_to_code",
                                  type=str,
                                  help="Path to code for StyleGAN 2 ADA.")
    parser_calc_fids.add_argument("--metrics_dir",
                                  type=str, required=True,
                                  help="Path to metrics directory.")
    parser_calc_fids.add_argument("--batch_size",
                                  type=int, default=32,
                                  help="Mini-batch size.")
    parser_calc_fids.add_argument("--from_epoch",
                                  type=int, default=0,
                                  help="Num epochs which model has been trained.")
    parser_calc_fids.add_argument("--dim_latent_space",
                                  type=int, default=100,
                                  help="Dimensionality of the latent space")
    parser_calc_fids.set_defaults(func=calc_fid_from_statistics)

    parser_all_calc_fids = subparsers.add_parser('calc_all_fid_and_intra_fid',
                                                 help="Calculate FID and Intra-FID for all models in models dir.")
    parser_all_calc_fids.add_argument("--dataset_statistics_dir",
                                      type=str, required=True,
                                      help="Statistics filepath.")
    parser_all_calc_fids.add_argument("--models_dir",
                                      type=str, required=True,
                                      help="Path to directory with models.")
    parser_all_calc_fids.add_argument("--model_type",
                                      type=str, required=True, choices=['cdcgan', 'pix2pix', 'stylegan2ada'],
                                      help="Type of generator.")
    parser_all_calc_fids.add_argument("--input_images_dir",
                                      type=str,
                                      help="Path to input images directory for pix2pix model.")
    parser_all_calc_fids.add_argument("--path_to_code",
                                  type=str,
                                  help="Path to code for StyleGAN 2 ADA.")
    parser_all_calc_fids.add_argument("--metrics_dir",
                                      type=str, required=True,
                                      help="Path to metrics directory.")
    parser_all_calc_fids.add_argument("--batch_size",
                                      type=int, default=32,
                                      help="Mini-batch size.")
    parser_all_calc_fids.add_argument("--dim_latent_space",
                                      type=int, default=100,
                                      help="Dimensionality of the latent space")
    parser_all_calc_fids.set_defaults(func=calc_all_fid_from_statistics)

    parser_calc_statistics = subparsers.add_parser('calc_dataset_statistics',
                                                   help="Calculate and save statistics from dataset images.")
    parser_calc_statistics.add_argument("--images_dir",
                                        type=str, required=True,
                                        help="Path to images.")
    parser_calc_statistics.add_argument("--output_dir",
                                        type=str, required=True,
                                        help="Directory where will be saved statistics.")
    parser_calc_statistics.add_argument("--batch_size",
                                        type=int, default=32,
                                        help="Mini-batch size.")
    parser_calc_statistics.set_defaults(func=calc_and_save_statistics)

    args = ap.parse_args()
    params = DotMap(vars(args))
    del params['func']
    print('Input params:')
    print(json.dumps(params, indent=2))

    del params['command']
    args.func(**params)

    return params


def main():
    process_input_arguments_and_execute_command()


if __name__ == '__main__':
    main()
