import json
import os.path
import random

import numpy as np
import pandas as pd
import tensorboard as tb
import tensorflow as tf
import torch
from dotmap import DotMap
from matplotlib import pyplot as plt
from tensorflow import keras

# os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
from training.dataset import calc_steps_per_epoch, NUM_IMAGES, get_filenames, LABELS, NUM_LABELS


def get_metric_data(df, metric, num_train_steps):
    df_metric = df[df['tag'] == metric]
    df_metric = df_metric[df_metric['step'] < num_train_steps].sort_values(by=['step'])
    df_metric = df_metric.drop_duplicates('step')
    print('Num {} data is {},'.format(metric, df_metric.size))
    return df_metric


def calc_best_values(best_values, data, steps, step_str, value_type, best_min):
    data = data.to_numpy()
    steps = steps.to_numpy()

    best_values[value_type]['value'] = float(data.min()) if best_min else float(data.max())
    best_values[value_type][step_str] = int(steps[data.argmin()]) if best_min else int(steps[data.argmax()])

    return best_values


def _plot_state(ax, xdata, ydata, title, xlabel, ylabel):
    ax.plot(xdata, ydata)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)


def save_all_data_in_one_figure(xdatas, ydatas, xlabels, ylabels, titles, path):
    nrows = 2
    ncols = 2
    fig, axs = plt.subplots(nrows, ncols, squeeze=False, figsize=(10, 8))

    pos = 0
    for row_pos in range(nrows):
        for col_pos in range(ncols):
            _plot_state(axs[row_pos, col_pos],
                        xdata=xdatas[pos], ydata=ydatas[pos], title=titles[pos],
                        xlabel=xlabels[pos], ylabel=ylabels[pos])
            pos = pos + 1

    plt.tight_layout()
    plt.savefig(path)


def save_all_images_in_one_figure(images, path):
    nrows = 3
    ncols = 3
    fig, axs = plt.subplots(nrows, ncols, squeeze=False, figsize=(10, 8))

    pos = 0
    for row_pos in range(nrows):
        for col_pos in range(ncols):
            axs[row_pos, col_pos].imshow(images[pos])
            axs[row_pos, col_pos].axis('off')
            pos = pos + 1

    plt.tight_layout()
    plt.savefig(path)


def save_data_from_tb_dev(experiment_id, path):
    # command for uploading data to tb dev
    # tensorboard dev upload --logdir batch_size_32_lr_0.0002/logs
    experiment = tb.data.experimental.ExperimentFromDev(experiment_id)

    df = pd.DataFrame(experiment.get_scalars())
    df.to_csv(os.path.join(path, 'scalars.csv'), index=False)


def generate_images_cdcgan(generator, num_img, num_labels, latent_dim=100):
    random_latent_vectors = tf.random.normal(shape=(num_img, latent_dim))
    random_labels = tf.random.uniform(shape=[num_img], minval=0, maxval=num_labels, dtype=tf.int64)
    generated_images = generator([random_latent_vectors, random_labels])

    # convert normalized image back: [-1, 1] -> [0, 1]
    generated_images = tf.multiply(generated_images, 0.5)
    generated_images = tf.add(generated_images, 0.5)

    return generated_images


def generate_images_pix2pix(filepaths, generator):
    input_images = []
    for filepath in filepaths:
        image = tf.io.read_file(filepath)
        image = tf.image.decode_jpeg(image)
        image = tf.cast(image, dtype=tf.float32)
        image = (image / 127.5) - 1
        input_images.append(image)

    input_images = tf.convert_to_tensor(input_images)

    generated_images = generator(input_images, training=True)
    generated_images = tf.multiply(generated_images, 0.5)
    generated_images = tf.add(generated_images, 0.5)

    return generated_images.numpy()


def create_summary_images_cdcgan(path_to_exp, epoch, num_labels):
    model = keras.models.load_model(os.path.join(path_to_exp, 'saved_models/epoch_{}.h5'.format(epoch)))
    images = generate_images_cdcgan(model, num_img=9, num_labels=num_labels)
    save_all_images_in_one_figure(images, os.path.join(path_to_exp, 'images_from_epoch_{}.pdf'.format(epoch)))


def create_summary_images_pix2pix(path_to_exp, epoch, input_images_dir):
    model = keras.models.load_model(os.path.join(path_to_exp, 'saved_models/epoch_{}.h5'.format(epoch)))
    filepaths = [os.path.join(input_images_dir, filename) for filename in get_filenames(input_images_dir)]
    random_filepaths = random.sample(filepaths, 9)
    images = generate_images_pix2pix(random_filepaths, model)
    save_all_images_in_one_figure(images, os.path.join(path_to_exp, 'images_from_epoch_{}.pdf'.format(epoch)))


def generate_images_stylegan2ada(batch_size, device, model):
    z = torch.randn([batch_size, model.z_dim]).to(device)
    c = torch.zeros([batch_size, model.c_dim]).to(device)
    for i in range(batch_size):
        c[i, random.randint(0, len(LABELS) - 1)] = 1
    images = model(z, c, truncation_psi=1, noise_mode='const', force_fp32=True)
    images = (images.permute(0, 2, 3, 1) * 127.5 + 128).clamp(0, 255).to(torch.float32)

    images = images.cpu().numpy()
    images = (images - np.min(images)) / (np.max(images) - np.min(images))
    return images


def create_summary_images_stylegan2ada(path_to_exp, epoch, stylegan2ada_project_dir):
    import sys
    sys.path.insert(0, stylegan2ada_project_dir)
    import dnnlib
    import legacy

    device = torch.device('cuda')
    with dnnlib.util.open_url(os.path.join(path_to_exp, 'saved_models', 'epoch_{}.pkl'.format(epoch))) as f:
        generator = legacy.load_network_pkl(f)['G_ema'].to(device)
    images = generate_images_stylegan2ada(9, device, generator)
    save_all_images_in_one_figure(images, os.path.join(path_to_exp, 'images_from_epoch_{}.pdf'.format(epoch)))


def create_summary(path, epochs, batch_size, num_images, d_loss_metric_name='d_loss', g_loss_metric_name='g_loss'):
    df = pd.read_csv(os.path.join(path, 'scalars.csv'))
    print('Columns of df are {}'.format(df.columns))

    metrics = df['tag'].unique()
    print('Metrics are {}'.format(metrics))

    num_train_steps = calc_steps_per_epoch(num_images, batch_size) * epochs
    print('Num train steps is {}.'.format(num_train_steps))

    datas_d_loss = get_metric_data(df, d_loss_metric_name, num_train_steps)
    datas_g_loss = get_metric_data(df, g_loss_metric_name, num_train_steps)
    datas_fid = get_metric_data(df, 'fid', num_train_steps)
    datas_intra_fid = get_metric_data(df, 'intra_fid', num_train_steps)

    xdatas = [datas_d_loss['step'], datas_g_loss['step'], datas_fid['step'], datas_intra_fid['step']]
    ydatas = [datas_d_loss['value'], datas_g_loss['value'], datas_fid['value'], datas_intra_fid['value']]
    xlabels = ['step', 'step', 'epoch', 'epoch']
    ylabels = ['loss', 'loss', 'fid', 'intra-fid']
    titles = ['Hodnoty loss funkce diskriminátoru', 'Hodnoty loss funkce generátoru', 'FID', 'Intra-FID']

    best_values = DotMap()
    calc_best_values(best_values, datas_d_loss['value'], datas_d_loss['step'], step_str='step', value_type='d_loss',
                     best_min=True)
    calc_best_values(best_values, datas_g_loss['value'], datas_g_loss['step'], step_str='step', value_type='g_loss',
                     best_min=False)
    calc_best_values(best_values, datas_fid['value'], datas_fid['step'], step_str='epoch', value_type='fid',
                     best_min=True)
    calc_best_values(best_values, datas_intra_fid['value'], datas_intra_fid['step'], step_str='epoch',
                     value_type='intra_fid',
                     best_min=True)

    with open(os.path.join(path, 'best_values.json'), 'w') as f:
        json.dump(best_values, f, indent=2)
    save_all_data_in_one_figure(xdatas, ydatas, xlabels, ylabels, titles,
                                os.path.join(path, 'summary_{}.pdf'.format(batch_size)))


def main():
    epochs = 60
    num_images = sum(NUM_IMAGES)
    path_to_exp = '.\\logs\\backup_00007-256_dataset_color_constancy_stylegan2ada_proccess-cond-auto1-kimg50_3'
    stylegan2ada_project_dir = '.\\stylegan2-ada-pytorch'
    # input_images_dir = ".\\256_dataset_color_cons_for_pix2pix_more_labels\\input_images"

    # create_summary(path_to_exp, epochs=epochs, batch_size=128, num_images=num_images, d_loss_metric_name='d_loss',
    #                g_loss_metric_name='g_loss')
    # create_summary(path_to_exp, epochs=epochs, batch_size=16, num_images=num_images, d_loss_metric_name='Loss/D/loss',
    #                g_loss_metric_name='Loss/G/loss')
    # create_summary_images_cdcgan(path_to_exp, epoch=11, num_labels=NUM_LABELS)
    # create_summary_images_pix2pix(path_to_exp, epoch=29, input_images_dir=input_images_dir)
    create_summary_images_stylegan2ada(path_to_exp, epoch=59, stylegan2ada_project_dir=stylegan2ada_project_dir)

    # save_data_from_tb_dev('MGY24Sg4Svu82XQGi3F0Wg', path_to_exp)


if __name__ == '__main__':
    main()
