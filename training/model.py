import tensorflow as tf
from tensorflow.keras import initializers
from tensorflow.keras.layers import Concatenate, Embedding, Input, Reshape, Activation, Conv2DTranspose, Conv2D, \
    LayerNormalization, BatchNormalization, Dense, \
    LeakyReLU, \
    Flatten


def create_generator(latent_dim):
    weight_init_stddev = 0.02
    alpha = 0.3

    label = Input(shape=[1])
    label_embedding = Embedding(input_dim=8, output_dim=50, input_length=8)(label)
    label_embedding = Dense(units=4 * 4 * 3)(label_embedding)
    label_embedding = Reshape(target_shape=(4, 4, 3))(label_embedding)

    noise = Input(shape=[latent_dim])
    x_from_noise = Dense(units=4 * 4 * 1024)(noise)
    x_from_noise = Reshape(target_shape=(4, 4, 1024))(x_from_noise)
    x_from_noise = LeakyReLU(alpha=alpha)(x_from_noise)

    x = Concatenate(axis=3)([x_from_noise, label_embedding])

    x = Conv2DTranspose(filters=512, kernel_size=[5, 5], strides=[2, 2], padding="same",
                        kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev))(x)

    x = BatchNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2DTranspose(filters=256, kernel_size=[5, 5], strides=[2, 2], padding="same",
                        kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev))(x)

    x = BatchNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2DTranspose(filters=128, kernel_size=[5, 5], strides=[2, 2], padding="same",
                        kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev))(x)

    x = BatchNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2DTranspose(filters=3, kernel_size=[5, 5], strides=[2, 2], padding="same",
                        kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev))(x)
    generated_image = Activation('tanh')(x)

    g_model = tf.keras.models.Model(inputs=[noise, label], outputs=generated_image, name="generator")

    return g_model


def create_discriminator(input_img_shape=(64, 64, 3)):
    weight_init_stddev = 0.02
    alpha = 0.3

    img_input = Input(shape=input_img_shape)

    label = Input(shape=[1])
    label_embedding = Embedding(input_dim=8, output_dim=50)(label)
    label_embedding = Dense(units=input_img_shape[0] * input_img_shape[1] * input_img_shape[2])(label_embedding)
    label_embedding = Reshape(target_shape=input_img_shape)(label_embedding)

    x = Concatenate(axis=3)([img_input, label_embedding])

    x = Conv2D(64, kernel_size=[5, 5], strides=[2, 2],
               kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev),
               padding="same")(x)

    x = LayerNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2D(128, kernel_size=[5, 5], strides=[2, 2],
               kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev),
               padding="same")(x)

    x = LayerNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2D(256, kernel_size=[5, 5], strides=[2, 2],
               kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev),
               padding="same")(x)

    x = LayerNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2D(512, kernel_size=[5, 5], strides=[2, 2],
               kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev),
               padding="same")(x)

    x = LayerNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Conv2D(1024, kernel_size=[5, 5], strides=[2, 2],
               kernel_initializer=initializers.RandomNormal(stddev=weight_init_stddev),
               padding="same")(x)

    x = LayerNormalization()(x)
    x = LeakyReLU(alpha=alpha)(x)
    x = Flatten()(x)
    dis_output = Dense(1)(x)

    d_model = tf.keras.models.Model(inputs=[img_input, label], outputs=dis_output, name="discriminator")

    return d_model
