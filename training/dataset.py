import os

import numpy as np
import tensorflow as tf

LABELS = ['MEL', 'NV', 'BCC', 'AK', 'BKL', 'DF', 'VASC', 'SCC']
LABEL_MAPPER = {
    0: 'MEL',
    1: 'NV',
    2: 'BCC',
    3: 'AK',
    4: 'BKL',
    5: 'DF',
    6: 'VASC',
    7: 'SCC'
}
NUM_IMAGES = [4522, 12875, 3323, 867, 2624, 239, 253, 628]
NUM_LABELS = len(LABELS)


@tf.function
def read_image(image_path):
    image = tf.io.read_file(image_path)
    image = tf.image.decode_image(image, channels=3, dtype=tf.uint8)
    return image


@tf.function
def normalize(image):
    # Map [0, 255] to [-1, 1].
    return (tf.cast(image, dtype=tf.float32) - 127.5) / 127.5


@tf.function
def preprocess(image_path, label):
    image = read_image(image_path)
    image = normalize(image)
    return image, label


def create_dataset_from_tensor_slices(image_names, labels, batch_size):
    ds = tf.data.Dataset.from_tensor_slices((image_names, labels))
    ds = ds.map(preprocess, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds = ds.shuffle(buffer_size=2048, reshuffle_each_iteration=True)
    ds = ds.batch(batch_size=batch_size)
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return ds


def create_dataset(batch_size, images_dir):
    filenames = [os.path.join(images_dir, filename) for filename in get_filenames(images_dir)]
    labels = []
    for filename in filenames:
        parts = filename.split('/')
        labels.append(int(parts[len(parts) - 1].split('_')[0]))

    return create_dataset_from_tensor_slices(
        filenames,
        labels,
        batch_size
    )


def get_filenames(dir_name):
    filenames = []
    for filename in os.listdir(dir_name):
        if os.path.isfile(os.path.join(dir_name, filename)):
            filenames.append(filename)
    return filenames


def get_filepaths_per_class(filepaths):
    filepaths_per_class = {clazz: [] for clazz in range(len(LABELS))}
    for filepath in filepaths:
        clazz = int(os.path.basename(filepath).split('_')[0])
        filepaths_per_class[clazz].append(filepath)
    return filepaths_per_class


def calc_steps_per_epoch(n, batch_size):
    return np.ceil(n / batch_size)
