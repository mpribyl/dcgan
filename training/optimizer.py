import tensorflow as tf

def create_adam_opt(lr, beta_1, beta_2):
    return tf.keras.optimizers.Adam(
        learning_rate=lr, beta_1=beta_1, beta_2=beta_2
    )