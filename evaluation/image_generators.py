import numpy as np
import tensorflow as tf
import torch

# import os
# os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"


class ImageGenerator:
    TARGET_WIDTH = 299
    TARGET_HEIGHT = 299

    def __init__(self, generator, inception_model):
        self.generator = generator
        self.inception_model = inception_model

    def generate_images(self):
        raise NotImplemented


class CdcganImageGenerator(ImageGenerator):
    def __init__(self, batch_size, label, dim_latent_space, generator, inception_model):
        super().__init__(generator=generator, inception_model=inception_model)
        self.batch_size = batch_size
        self.label = label
        self.dim_latent_space = dim_latent_space

    def generate_images(self):
        latents = tf.random.normal(shape=[self.batch_size, self.dim_latent_space])
        labels = tf.fill(value=self.label, dims=(self.batch_size,))
        images = self.generator([latents, labels])
        images = tf.multiply(images, 0.5)
        images = tf.add(images, 0.5)
        images = tf.image.resize_with_crop_or_pad(images,
                                                  target_width=ImageGenerator.TARGET_WIDTH,
                                                  target_height=ImageGenerator.TARGET_HEIGHT)
        return self.inception_model(images)


class PixToPixImageGenerator(ImageGenerator):
    def __init__(self, batch_size, filepaths, generator, inception_model):
        super().__init__(generator=generator, inception_model=inception_model)
        self.filepaths_batches = [filepaths[i:i + batch_size] for i in range(0, len(filepaths), batch_size)]
        self.cur_batch = 0

    def generate_images(self):
        filepaths = self.filepaths_batches[self.cur_batch]
        self.cur_batch = self.cur_batch + 1

        input_images = []
        for filepath in filepaths:
            image = tf.io.read_file(filepath)
            image = tf.image.decode_jpeg(image)
            image = tf.cast(image, dtype=tf.float32)
            image = (image / 127.5) - 1
            input_images.append(image)

        input_images = tf.convert_to_tensor(input_images)

        generated_images = self.generator(input_images, training=True)
        generated_images = tf.multiply(generated_images, 0.5)
        generated_images = tf.add(generated_images, 0.5)

        generated_images = tf.image.resize_with_crop_or_pad(generated_images,
                                                            target_width=ImageGenerator.TARGET_WIDTH,
                                                            target_height=ImageGenerator.TARGET_HEIGHT)

        return self.inception_model(generated_images)


class Stylegan2AdaImageGenerator(ImageGenerator):
    def __init__(self, batch_size, label, generator, inception_model):
        super().__init__(generator=generator, inception_model=inception_model)
        self.batch_size = batch_size
        self.label = label
        self.device = torch.device('cuda')

    def generate_images(self):
        z = torch.randn([self.batch_size, self.generator.z_dim]).to(self.device)
        c = torch.zeros([self.batch_size, self.generator.c_dim]).to(self.device)
        c[:, self.label] = 1
        images = self.generator(z, c, truncation_psi=1, noise_mode='const', force_fp32=True)
        images = (images.permute(0, 2, 3, 1) * 127.5 + 128).clamp(0, 255).to(torch.float32)

        input_images = images.cpu().numpy()
        input_images = (input_images - np.min(input_images)) / (np.max(input_images) - np.min(input_images))
        input_images = tf.convert_to_tensor(input_images)
        input_images = tf.image.resize_with_crop_or_pad(input_images,
                                                        target_width=ImageGenerator.TARGET_WIDTH,
                                                        target_height=ImageGenerator.TARGET_HEIGHT)

        return self.inception_model(input_images)


def create_image_generator(model_type, batch_size, label, dim_latent_space, generator, inception_model, filepaths):
    if model_type == 'cdcgan':
        image_generator = CdcganImageGenerator(batch_size, label, dim_latent_space, generator, inception_model)
    elif model_type == 'pix2pix':
        image_generator = PixToPixImageGenerator(batch_size, filepaths, generator, inception_model)
    elif model_type == 'stylegan2ada':
        image_generator = Stylegan2AdaImageGenerator(batch_size, label, generator, inception_model)
    else:
        raise Exception('Unknown input value for param model_type {}.'.format(model_type))

    return image_generator
