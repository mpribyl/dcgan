import numpy as np

from training.dataset import LABELS


def _load_statistics(statistics_path):
    with np.load(statistics_path) as f:
        m, sigma = f['m'][:], f['sigma'][:]
    return m, sigma

def get_filenames_per_class(filenames):
    filenames_per_class = {clazz: [] for clazz in range(len(LABELS))}
    for filename in filenames:
        clazz = int(filename.split('_')[0])
        filenames_per_class[clazz].append(filename)
    return filenames_per_class
