import os
import time
from datetime import datetime

import numpy as np
import scipy
import tensorflow as tf
from tensorflow.keras.applications.inception_v3 import InceptionV3

from evaluation.image_generators import create_image_generator
from evaluation.util import _load_statistics, get_filenames_per_class
from training.dataset import NUM_IMAGES, LABELS, get_filenames

INCEPTION_LAST_LAYER_SIZE = 2048


def calc_fid(mu_fake, mu_real, sigma_fake, sigma_real):
    m = np.square(mu_fake - mu_real).sum()
    s, _ = scipy.linalg.sqrtm(np.dot(sigma_fake, sigma_real), disp=False)
    dist = m + np.trace(sigma_fake + sigma_real - 2 * s)
    return np.real(dist)


def calc_fid_and_get_activations_for_class(inception_model, batch_size, generator, dim_latent_space, num_fakes,
                                           real_statistics_path, label, model_type, filepaths):
    mu_real, sigma_real = _load_statistics(real_statistics_path)

    n_batches = int(np.ceil(num_fakes / batch_size))
    activations = np.ndarray((num_fakes, INCEPTION_LAST_LAYER_SIZE))

    images_generator = create_image_generator(
        model_type=model_type,
        batch_size=batch_size,
        label=label,
        dim_latent_space=dim_latent_space,
        generator=generator,
        inception_model=inception_model,
        filepaths=filepaths
    )

    for batch in range(n_batches):
        start_idx = batch_size * batch
        end_idx = batch_size * (batch + 1)

        if end_idx > num_fakes:
            end_idx = num_fakes

        activations_of_generated_images = images_generator.generate_images().numpy()
        activations[start_idx:end_idx, :] = activations_of_generated_images[:end_idx - start_idx]

    mu_fake = np.mean(activations, axis=0)
    sigma_fake = np.cov(activations, rowvar=False)

    fid = calc_fid(mu_fake, mu_real, sigma_fake, sigma_real)
    print('Calculated FID: {}'.format(fid))
    return fid, activations


def calc_all_fid_from_statistics(models_dir, model_type, dataset_statistics_dir, metrics_dir, batch_size,
                                 dim_latent_space, input_images_dir, path_to_code):
    model_filenames = get_filenames(models_dir)
    for model_filename in model_filenames:
        models_filepath = os.path.join(models_dir, model_filename)
        from_epoch = int(model_filename.split('.')[0].split('_')[1])
        calc_fid_from_statistics(
            models_filepath=models_filepath,
            model_type=model_type,
            dataset_statistics_dir=dataset_statistics_dir,
            metrics_dir=metrics_dir,
            batch_size=batch_size,
            dim_latent_space=dim_latent_space,
            input_images_dir=input_images_dir,
            path_to_code=path_to_code,
            from_epoch=from_epoch
        )


def calc_fid_from_statistics(models_filepath, model_type, dataset_statistics_dir, metrics_dir, batch_size,
                             dim_latent_space, input_images_dir, path_to_code,
                             from_epoch):
    inception_model = InceptionV3(include_top=False, pooling='avg', input_shape=(299, 299, 3))
    if model_type == 'cdcgan' or model_type == 'pix2pix':
        generator = tf.keras.models.load_model(models_filepath, compile=False)
    elif model_type == 'stylegan2ada':
        import sys
        sys.path.insert(0, path_to_code)
        import torch
        import dnnlib
        import legacy

        device = torch.device('cuda')
        with dnnlib.util.open_url(models_filepath) as f:
            generator = legacy.load_network_pkl(f)['G_ema'].to(device)
    else:
        raise Exception('Unknown input value for param model_type {}.'.format(model_type))

    log_dir = os.path.join(metrics_dir, datetime.now().strftime("%Y_%m_%d-%H_%M_%S"))
    file_writer = tf.summary.create_file_writer(log_dir)

    print('Computing FID and all class FID for generator {}:'.format(models_filepath))
    fid_start_time = time.time()

    all_activations = np.ndarray(shape=[sum(NUM_IMAGES), INCEPTION_LAST_LAYER_SIZE])
    position = 0

    filenames_per_class = get_filenames_per_class(get_filenames(input_images_dir)) if input_images_dir else None
    class_fids = []
    for clazz in range(len(LABELS)):
        num_images_for_class = NUM_IMAGES[clazz]
        filenames = filenames_per_class[clazz] if input_images_dir else []
        class_fid_start_time = time.time()
        class_fid, activations = calc_fid_and_get_activations_for_class(
            inception_model=inception_model,
            batch_size=batch_size,
            generator=generator,
            dim_latent_space=dim_latent_space,
            num_fakes=num_images_for_class,
            real_statistics_path=os.path.join(dataset_statistics_dir, 'statistics_for_class_{}.npz'.format(clazz)),
            label=clazz,
            model_type=model_type,
            filepaths=[os.path.join(input_images_dir, filename) for filename in filenames]
        )
        print('FID for class {} is {}.'.format(clazz, class_fid))
        print('FID for class {} was calculated after {} sec.'.format(clazz, time.time() - class_fid_start_time))
        class_fids.append(class_fid)
        with file_writer.as_default():
            tf.summary.scalar('fid_{}'.format(clazz), class_fid, step=from_epoch)

        all_activations[position:position + num_images_for_class, :] = activations
        position = position + num_images_for_class

    intra_fid = np.mean(class_fids)
    print('Intra-FID is {}'.format(intra_fid))
    with file_writer.as_default():
        tf.summary.scalar('intra_fid', intra_fid, step=from_epoch)

    mu_fake = np.mean(all_activations, axis=0)
    sigma_fake = np.cov(all_activations, rowvar=False)
    mu_real, sigma_real = _load_statistics(os.path.join(dataset_statistics_dir, 'statistics.npz'))
    fid = calc_fid(mu_fake, mu_real, sigma_fake, sigma_real)
    print('FID is {}'.format(fid))
    with file_writer.as_default():
        tf.summary.scalar('fid', fid, step=from_epoch)

    print('Calculated FIDs exec time was {} sec'.format(int(time.time() - fid_start_time)))
    print('FID and all class FID were calculated')
