import os
import time

import numpy as np
import tensorflow as tf
from tensorflow.keras.applications.inception_v3 import InceptionV3

from evaluation.fid import INCEPTION_LAST_LAYER_SIZE
from evaluation.util import get_filenames_per_class
from training.dataset import NUM_IMAGES, LABELS, get_filenames


def get_activations(inception_model, batch_size, images_dir, filenames):
    @tf.function
    def read_and_process_image(img_path):
        image = tf.io.read_file(img_path)
        image = tf.image.decode_image(image, channels=3, dtype=tf.float32)
        image = tf.image.resize_with_crop_or_pad(image, target_width=299, target_height=299)
        return inception_model(tf.expand_dims(image, axis=0))

    images_ds = tf.data.Dataset.from_tensor_slices([os.path.join(images_dir, filename) for filename in filenames]) \
        .map(lambda image: read_and_process_image(image)) \
        .batch(batch_size=batch_size)
    images_iterator = iter(images_ds)

    n_batches = int(np.ceil(len(filenames) / batch_size))
    activations = np.ndarray((len(filenames), INCEPTION_LAST_LAYER_SIZE))
    for batch in range(n_batches):
        start_idx = batch_size * batch
        end_idx = batch_size * (batch + 1)

        if end_idx > len(filenames):
            end_idx = len(filenames)

        images = next(images_iterator)
        activations[start_idx:end_idx, :] = images.numpy().reshape(images.shape[0], -1)

    return activations


def calc_and_save_statistics(images_dir, output_dir, batch_size):
    inception_model = InceptionV3(include_top=False, pooling='avg', input_shape=(299, 299, 3))

    print('Computing statistics:')
    start_time = time.time()

    all_activations = np.ndarray(shape=[sum(NUM_IMAGES), INCEPTION_LAST_LAYER_SIZE])
    position = 0

    filenames_per_class = get_filenames_per_class(get_filenames(images_dir))

    for clazz in range(len(LABELS)):
        filenames_with_class = filenames_per_class[clazz]
        class_start_time = time.time()
        activations = get_activations(
            inception_model=inception_model,
            batch_size=batch_size,
            images_dir=images_dir,
            filenames=filenames_with_class
        )
        mu = np.mean(activations, axis=0)
        sigma = np.cov(activations, rowvar=False)
        filepath = os.path.join(output_dir, 'statistics_for_class_{}.npz'.format(clazz))
        os.makedirs(os.path.dirname(filepath), exist_ok=True)
        np.savez(filepath, m=mu, sigma=sigma)
        print('Statistics for class {} was calculated and saved after {} sec.'
              .format(clazz, time.time() - class_start_time))

        all_activations[position:position + len(filenames_with_class), :] = activations
        position = position + len(filenames_with_class)

    mu = np.mean(all_activations, axis=0)
    sigma = np.cov(all_activations, rowvar=False)
    filepath = os.path.join(output_dir, 'statistics.npz')
    os.makedirs(os.path.dirname(filepath), exist_ok=True)
    np.savez(filepath, m=mu, sigma=sigma)
    print('Statistics was calculated and saved after {} sec.'.format(time.time() - start_time))
