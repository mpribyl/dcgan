import argparse
import json
import os
import time
from datetime import datetime

import tensorflow as tf
from dotmap import DotMap

from training.dataset import create_dataset, calc_steps_per_epoch, get_filenames, NUM_LABELS
from training.loss import generator_loss, discriminator_loss, gradient_penalty
from training.model import create_discriminator, create_generator
from training.optimizer import create_adam_opt


def process_training_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    group = ap.add_argument_group('dataset options')
    group.add_argument("--images_dir",
                       type=str, required=True,
                       help="Path to images")

    group = ap.add_argument_group('training options')
    group.add_argument("--batch_size",
                       type=int, default=32,
                       help="Size of training batch size")
    group.add_argument("--dim_latent_space",
                       type=int, default=100,
                       help="Dimensionality of the latent space")
    group.add_argument("--gen_lr",
                       type=float, default=0.0001,
                       help="Learning rate of generator's optimizer")
    group.add_argument("--dis_lr",
                       type=float, default=0.0001,
                       help="Learning rate of discriminator's optimizer")
    group.add_argument("--epochs",
                       type=int, default=1,
                       help="How much epochs train the model.")
    group.add_argument("--log_dir",
                       type=str, default='./logs/',
                       help="Logging directory.")
    group.add_argument("--generate_images_freq",
                       type=int, default=100,
                       help="Generate images every x-th batch.")
    group.add_argument("--saved_models_dir",
                       type=str, default='./saved_models/',
                       help="Directory where will be saved models.")
    group.add_argument("--save_model_freq",
                       type=int, default=100,
                       help="Saves model every x-th batch.")
    group.add_argument("--saved_checkpoints_dir",
                       type=str, default='./saved_checkpoints/',
                       help="Directory where will be saved checkpoints.")
    group.add_argument("--save_checkpoint_freq",
                       type=int, default=100,
                       help="Saves checkpoint every x-th batch.")
    group.add_argument("--load_checkpoint",
                       type=str, default=None,
                       help="Path to checkpoint which will be load.")

    return DotMap(vars(ap.parse_args()))


def create_training_params(input_args) -> DotMap:
    params = DotMap(input_args)

    params.steps_per_epoch = calc_steps_per_epoch(len(get_filenames(params.images_dir)), params.batch_size)

    params_for_print = params
    print('Training params:')
    print(json.dumps(params_for_print, indent=2))

    return params


def save_generated_images(file_writer, generator, step, latent_dim, num_img=10, train_step_type='batch'):
    random_latent_vectors = tf.random.normal(shape=(num_img, latent_dim))
    random_labels = tf.random.uniform(shape=[num_img], minval=0, maxval=NUM_LABELS, dtype=tf.int64)
    generated_images = generator([random_latent_vectors, random_labels])

    # convert normalized image back: [-1, 1] -> [0, 1]
    generated_images = tf.multiply(generated_images, 0.5)
    generated_images = tf.add(generated_images, 0.5)

    with file_writer.as_default():
        tf.summary.image("generated_images_after_" + train_step_type + '_' + str(step), generated_images,
                         step=step, max_outputs=num_img)


def train_step_wrapper(
        generator, discriminator,  # models
        gen_loss_fn, dis_loss_fn,  # loss functions
        gen_opt, dis_opt,  # optimizers
        dim_latent_space=100, d_steps=5, gp_weight=10):

    @tf.function
    def train_step(input_data):
        real_images = input_data[0]
        real_labels = input_data[1]
        batch_size = tf.shape(real_images)[0]

        for i in range(d_steps):
            random_latent_vectors = tf.random.normal(shape=(batch_size, dim_latent_space))
            with tf.GradientTape() as tape:
                fake_images = generator([random_latent_vectors, real_labels], training=True)
                fake_logits = discriminator([fake_images, real_labels], training=True)
                real_logits = discriminator([real_images, real_labels], training=True)

                d_cost = dis_loss_fn(real_img=real_logits, fake_img=fake_logits)
                gp = gradient_penalty(discriminator, batch_size, real_images, fake_images, real_labels)
                d_loss = d_cost + gp * gp_weight

            d_gradient = tape.gradient(d_loss, discriminator.trainable_variables)
            dis_opt.apply_gradients(
                zip(d_gradient, discriminator.trainable_variables)
            )

        random_latent_vectors = tf.random.normal(shape=(batch_size, dim_latent_space))
        with tf.GradientTape() as tape:
            generated_images = generator([random_latent_vectors, real_labels], training=True)
            gen_img_logits = discriminator([generated_images, real_labels], training=True)
            g_loss = gen_loss_fn(gen_img_logits)

        gen_gradient = tape.gradient(g_loss, generator.trainable_variables)
        gen_opt.apply_gradients(
            zip(gen_gradient, generator.trainable_variables)
        )

        return {"d_loss": d_loss, "g_loss": g_loss}

    return train_step


def main():
    input_args = process_training_input_arguments()
    params = create_training_params(input_args)

    train_ds = create_dataset(
        params.batch_size,
        params.images_dir
    )

    generator = create_generator(params.dim_latent_space)
    discriminator = create_discriminator()

    gen_opt = create_adam_opt(lr=params.gen_lr, beta_1=0.5, beta_2=0.9)
    dis_opt = create_adam_opt(lr=params.dis_lr, beta_1=0.5, beta_2=0.9)

    checkpoint = tf.train.Checkpoint(
        step=tf.Variable(0),
        generator=generator,
        discriminator=discriminator,
        gen_opt=gen_opt,
        dis_opt=dis_opt,
        epoch_iter=iter(train_ds)
    )

    if params.load_checkpoint:
        checkpoint.restore(params.load_checkpoint)

    cur_epoch = int(int(checkpoint.step) / len(train_ds))

    manager = tf.train.CheckpointManager(checkpoint, params.saved_checkpoints_dir, max_to_keep=10)

    log_dir = os.path.join(params.log_dir, datetime.now().strftime("%Y_%m_%d-%H_%M_%S"))
    file_writer = tf.summary.create_file_writer(log_dir)

    train_step_fn = train_step_wrapper(
                generator, discriminator,
                generator_loss, discriminator_loss,
                gen_opt, dis_opt,
                dim_latent_space=params.dim_latent_space
    )

    d_loss_logs = []
    g_loss_logs = []
    while cur_epoch < params.epochs:
        epoch_start = time.time()

        if int(checkpoint.step) % len(train_ds) == 0 and int(checkpoint.step) != 0:
            checkpoint.epoch_iter = iter(train_ds)

        for image_batch in checkpoint.epoch_iter:
            step_start = time.time()
            logs = train_step_fn(image_batch)

            d_loss_logs.append(logs['d_loss'])
            g_loss_logs.append(logs['g_loss'])
            with file_writer.as_default():
                tf.summary.scalar('d_loss', d_loss_logs[len(d_loss_logs) - 1], step=int(checkpoint.step))
                tf.summary.scalar('g_loss', g_loss_logs[len(g_loss_logs) - 1], step=int(checkpoint.step))
            print('------step: {}, time: {:.2f} sec, d_loss: {}, g_loss: {}'
                  .format(int(checkpoint.step), time.time() - step_start, d_loss_logs[len(d_loss_logs) - 1],
                          g_loss_logs[len(g_loss_logs) - 1]))

            if int(checkpoint.step) % params.generate_images_freq == 0:
                save_generated_images(file_writer, generator, int(checkpoint.step), params.dim_latent_space)

            checkpoint.step.assign_add(1)

            if int(checkpoint.step) % params.save_checkpoint_freq == 0 and int(checkpoint.step) != 0:
                save_path = manager.save(checkpoint_number=int(checkpoint.step))
                print("saved checkpoint for step {}: {}".format(int(checkpoint.step), save_path))

        print('epoch: {}, time: {:.2f} sec, d_loss: {}, g_loss: {}'
              .format(cur_epoch, time.time() - epoch_start, d_loss_logs[len(d_loss_logs) - 1], g_loss_logs[len(g_loss_logs) - 1]))
        if cur_epoch % params.save_model_freq == 0:
            generator.save(os.path.join(params.saved_models_dir, 'epoch_{}.h5'.format(cur_epoch)), save_format='h5')
            print("saved generator for epoch {}: {}"
                  .format(cur_epoch, os.path.join(params.saved_models_dir, 'epoch_{}'.format(cur_epoch))))
        print('-------------------------------------')

        checkpoint.epoch_iter = iter(train_ds)
        cur_epoch = int(int(checkpoint.step) / len(train_ds))


if __name__ == '__main__':
    main()
